# 前端安全

# TODO

### XSS 攻击

跨站脚本攻击（Cross-site scripting），是代码注入的一种。

如何预防：
* 最普遍的做法是转义输入输出的内容，对于引号，尖括号，斜杠进行转义


### CSRF 攻击

跨站请求伪造（Cross-site request forgery），利用用户的登录态发起恶意请求。

如何预防：
* 要求server端加入CSRF的处理方法（至少在关键页面加入）；

### [返回主页](/README.md)