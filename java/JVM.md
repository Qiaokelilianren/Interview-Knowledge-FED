# JAVA 虚拟机

JVM用来运行javac编译的.class字节码文件

java的垃圾回收机制是在JVM上实现的。

垃圾回收机制和V8引擎类似，详情见[JS垃圾回收机制](/browser/garbage_collection.md)

JDK / JRE / JVM 的关系图（盗）
![虚拟机](/img/jdk.png)


### [返回主页](/README.md)